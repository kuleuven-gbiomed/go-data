FROM ubuntu:20.04
RUN apt-get update --fix-missing
RUN apt-get install -y lsof curl

COPY . /app
WORKDIR /app

EXPOSE 8000

RUN ./go-data-x64.sh

HEALTHCHECK --interval=5s --timeout=3s --retries=10 CMD curl --fail http://localhost:8000/ || exit 1

CMD ./go-data-x64--no-daemon.sh
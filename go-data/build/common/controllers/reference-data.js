"use strict";const app=require("../../server/server"),async=require("async"),importableFileHelpers=require("./../../components/importableFile");module.exports=function(e){e.getAvailableCategories=function(r){r(null,e.availableCategories)},e.exportFilteredReferenceData=function(e,r,t,n){e=e||{},app.utils.remote.helpers.exportFilteredModelsList(app,app.models.referenceData,{},e,r,"Reference Data",null,[],[],t,function(e){return new Promise(function(r,n){const a=app.utils.remote.getUserFromOptions(t);app.models.language.getLanguageDictionary(a.languageId,function(t,a){if(t)return n(t);e.forEach(function(e){e.categoryId=a.getTranslation(e.categoryId),e.value=a.getTranslation(e.value),e.description=a.getTranslation(e.description)}),r(e)})})},n)},e.importImportableReferenceDataFileUsingMap=function(r,t,n){t._sync=!1,importableFileHelpers.getTemporaryFileById(r.fileId).then(a=>{const i=a.data,o=app.utils.helpers.convertBooleanProperties(e,app.utils.helpers.remapProperties(i,r.map,r.valuesMap)),l=[],s=[];s.toString=function(){return JSON.stringify(this)},o.forEach(function(e,r){l.push(function(n){return app.utils.dbSync.syncRecord(t.remotingContext.req.logger,app.models.referenceData,e,t).then(function(e){n(null,e.record)}).catch(function(t){s.push({message:`Failed to import reference data ${r+1}`,error:t,recordNo:r+1,data:{file:i[r],save:e}}),n(null,null)})})}),async.parallelLimit(l,10,function(e,r){return e?n(e):s.length?((r=r.filter(e=>null!==e)).toString=function(){return JSON.stringify(this)},n(app.utils.apiError.getError("IMPORT_PARTIAL_SUCCESS",{model:app.models.referenceData.modelName,failed:s,success:r}))):void n(null,r)})}).catch(n)}};